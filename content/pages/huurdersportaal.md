---
__id: 2
icon: huurdersportaal.png
title: Huurdersportaal
text_1: Via het <a class="bold" target="_blank" href="https://login.cornerstonevastgoed.nl/">huurdersportaal</a> kan je inloggen in je eigen omgeving waar je de nodige documentatie betreffende het gehuurde kan vinden. Foto’s, stand van zaken van werkzaamheden, huurovereenkomst en de communicatie zijn hier terug te vinden. Op deze manier ontstaat er een transparant inzicht en kan wellicht al antwoord worden geven op eventuele vragen. Check dit portaal zo nu en dan, zodat je op de hoogte blijft.
---
