---
__id: 4
icon: faq.png
title: Faq
onderwerp_1: Onderhoud en reparatie
questions_1:
  [
    {
      _id: 0,
      question: In mijn woning is onderhoud of een reparatie noodzakelijk&comma; wie lost dit op?,
      answer: Controleer hiervoor de <a class="bold" target="_blank" href="/documents/cornerstone_huurwijzer.pdf">huurwijzer</a>. Hierin staat welke onderhouds-issues voor de verantwoording van de verhuurder valt en welke voor u als huurder.,
    },
    {
      _id: 1,
      question: In mijn woning is onderhoud of een reparatie noodzakelijk&comma; welke -aan de hand van de <a class="bold" target="_blank" href="/documents/cornerstone_huurwijzer.pdf">huurwijzer</a>- onder de verantwoordelijkheid van verhuurder valt. Hoe kan ik dit melden?,
      answer: U kunt via het <a target="_blank" href="https://login.cornerstonevastgoed.nl/">huurdersportaal</a> een melding maken. Via het <a target="_blank" href="https://login.cornerstonevastgoed.nl/">huurdersportaal</a> kunt u de status van de melding volgen.,
    },
    {
      _id: 2,
      question: Ik wil een bouwkundige verandering in mijn huurwoning aanbrengen&comma; mag dat?,
      answer:
        Bouwkundige veranderingen aan de woning zijn niet toegestaan zonder toestemming van Cornerstone Vastgoed. Denk bij bouwkundige veranderingen aan het doorbreken van muren&comma;
        verwijderen van deuren&comma; aanpassingen aan de keuken en/of badkamer. Pas wanneer er explicitiet toestemming van Cornerstone Vastgoed is gegeven mogen er bouwkundige veranderingen worden toepgepast.,
    },
  ]

onderwerp_2: Bewoning
questions_2:
  [
    {
      _id: 3,
      question: Ik heb een vaste relatie en wil met mijn partner gaan samenwonen. Wat moet ik doen?,
      answer: U kunt een inwonersverklaring aanvragen bij ons. Hiervoor zullen wij documenten opvragen welke wij nodig hebben om verklaring op te stellen.,
    },
    {
      _id: 4,
      question: Mijn medehuurder vertrekt uit de woning en ik wil graag blijven. Wat zijn hier de mogelijkheden?,
      answer: Wanneer een medehuurder wenst te vertrekken zullen wij moeten toetsen of u de gehele huursom kan dragen. Hiervoor zal aan de hand van een aantal gegevens een controle worden uitgevoerd en -indien van toepassing- kan er opnieuw een huurpaspoort worden aangevraagd.,
    },
    {
      _id: 5,
      question: Mijn partner vertrekt uit de woning n.a.v. een echtscheiding/ontbinding geregistreerd partnerschap.,
      answer: Wanneer uw partner vertrekt zullen wij moeten toetsen in of de u de gehele huursom kan dragen. Hiervoor zal aan de hand van een aantal gegevens een controle worden uitgevoerd en -indien van toepassing- kan er opnieuw een huurpaspoort worden aangevraagd.,
    },
  ]

onderwerp_3: Financieel
questions_3:
  [
    {
      _id: 6,
      question: Op welk rekeningnummer maak ik mijn huur over?,
      answer: Het rekeningnummer waar u de huur op betaald staat in de huurovereenkomst Wij adviseren om gebruik te maken van een automatisch incasso&comma; zodat de huursom tijdig wordt ontvangen.,
    },
    {
      _id: 7,
      question: Wanneer moet ik mijn huur betalen?,
      answer: De huur dient voor de 1ste van de maand op de in de huurovereenkomst aangegeven rekening te staan.,
    },
    {
      _id: 8,
      question: Door omstandigheden kan ik voor de aankomende maand niet tijdig betalen&comma; wat moet ik doen?,
      answer: Neem contact met ons op zodat wij op de hoogte zijn en wellicht naar een oplossing kunnen zoeken. Wanneer wij tijdig weten wat er speelt kunnen er afspraken gemaakt worden.,
    },
  ]

onderwerp_4: Huurbeëindiging
questions_4:
  [
    {
      _id: 9,
      question: Ik ga verhuizen&comma; hoe zeg ik de huur op?,
      answer: Voor het beëindigen van de huur stuurt u een email of een melding via het <a target="_blank" href="https://login.cornerstonevastgoed.nl/">huurdersportaal</a> voor het begin van de maand welke als laatste maand zal gelden. De opzegtermijn bedraagt doorgaans een volledige maand. Voorbeeld als u de huurovereenkomst per 1 november beëindigen&comma; dan dienen wij de aankondiging voor 1 oktober in het bezit te hebben. Hierin is oktober dus de laatste maand waarin u huurt.,
    },
    {
      _id: 10,
      question: Ik ga verhuizen&comma; hoe moet ik de woning opleveren aan het einde van de overeenkomst?,
      answer: Bij onze bevestiging van de opzegging ontvangt u een document waar het proces voor het opleveren van de woning in staat beschreven. Ook zullen er een twee afspraken met u worden gemaakt&comma; één voor de voorinspectie en één voor de eindinspectie. Tijdens de voorinspectie zal de inspecteur tezamen met u de punten opstellen welke dienen te worden herstelt&comma; mocht dit van toepassing zijn. De beschreven punten zullen ten tijde van de eindinspectie worden gecontroleerd. De openstaande punten tijdens de eindinspectie zullen leidend zijn&comma; mede gezien er tijdens de voorinspectie niet alles kan worden opgenomen.,
    },
    {
      _id: 11,
      question: Wanneer kan ik mijn waarborgsom terugverwachten?,
      answer: Wanneer u de woning correct heeft opgeleverd wordt de waarborgsom binnen 4 weken op uw rekening gestort. Wanneer er werkzaamheden in de woning nog dienen te worden herstelt na de eindinspectie zal dit langer duren. Nadat het herstel is uitgevoerd worden deze werkzaamheden verrekend met de waarborgsom.,
    },
  ]

onderwerp_5: Zorgplicht
questions_5:
  [
    {
      _id: 12,
      question: Wat is Zorgplicht en wat betekent dit voor mij?,
      answer: Op de verhuurder rust de zorgplicht om ervoor te zorgen dat het gebruik van het pand in overeenstemming is met overheidsregelgeving en de afspraken in de huurovereenkomst.<br /><br />Verhuurders dienen in Nederland serieus werk te maken van deze zorgplicht. Het enkele feit dat in de huurovereenkomst is afgesproken dat het pand niet illegaal mag worden gebruikt en dat de huurder de verantwoordelijkheid draagt voor overtredingen&comma; is voor de wetgever onvoldoende. De rechter heeft ook bepaald dat eerbiediging van de persoonlijke levenssfeer van de huurder&comma; de periodieke controle van het gebruik van het pand door de verhuurder niet in de weg staat.<br /> <br /> Indien verhuurder niet voldoet aan zijn wettelijke zorgplicht kan hem een forse bestuurlijke boete worden opgelegd. Het recht om boetes op te leggen ligt bij de gemeentelijke overheden; deze controleren bijzonder intensief op afwijkend gebruik. Deze door de wetgever opgelegde zorgplicht verplicht de verhuurder zich te informeren over het gebruik van het gehuurde. Dit om vast te stellen dat het gehuurde wordt gebruikt in overeenstemming met de overheidsregelgeving en de bepalingen in de huurovereenkomst.,
    },
    {
      _id: 13,
      question: Ben ik verplicht om aan de zorgplichtbezoeken mee te werken?,
      answer: Ja&comma; u bent verplicht te allen tijde medewerking te verlenen.,
    },
    {
      _id: 14,
      question: Ik heb een aankondiging van een zorgplichtbezoeken ontvangen &comma; maar ik ben dan niet aanwezig. Hoe kan ik de afspraak wijzigen?,
      answer: Mocht de genoemde datum of tijd niet uitkomen&comma; dan kunt u deze afspraak tot 48 uur van tevoren per e-mail of telefonisch kosteloos wijzigen. Mocht u niet binnen de gestelde termijn annuleren of wijzigen&comma; of bent u bij de gemaakte afspraak niet aanwezig&comma; dan worden de gemaakte voorrijkosten van € 40&comma;00 (excl. btw) aan u berekend. U kunt ons bereiken van maandag tot en met vrijdag van 09:00 tot 17:00 uur op telefoonnummer <a class="bold" href="tel:085-4884 780">085-4884 780</a> (keuze 1) en via het e-mailadres <a class="bold" href="mailto:zorgplicht@cornerstonevastgoed.nl">zorgplicht@cornerstonevastgoed.nl</a>.,
    },
    {
      _id: 15,
      question: Wanneer is er sprake van afwijkende bewoning?,
      answer: De woning de gehele duur van de huurovereenkomst daadwerkelijk&comma; geheel en voortdurend behoorlijk door de contractuele huurder(s). U bent niet bevoegd om het gehuurde geheel of gedeeltelijk in huur&comma; onderhuur of gebruik aan derden af te staan&comma; daar mede onder begrepen het verhuren van kamers&comma; het verlenen van pension&comma; het (tijdelijk) in gebruik te geven (zoals via AirBnB of een daarmee vergelijkbare organisatie)&comma; of het doen van afstand van huur.,
    },
    {
      _id: 16,
      question: Wanneer is er sprake van overbewoning?,
      answer: Voor de wijk waarin uw woning is gelegen&comma; heeft de gemeente bepaald dat een zelfstandige wooneenheid bewoond mag worden door een duurzaam gemeenschappelijk huishouden (bijv. een gezin)&comma; dan wel door maximaal 3 personen. Is dit niet het geval en zijn er naast uzelf&comma; met uw uitdrukkelijke instemming nog 3 personen ingeschreven in de Basisregistratie Personen&comma; bent u in overtreding. De gemeente heeft in haar huisvestingsverordening opgenomen dat&comma; wanneer aan meer dan 3 huurders wordt verhuurd&comma; een omzettingsvergunning is vereist. Vindt er controle plaats en blijken er meer dan drie gebruikers te zijn terwijl hiervoor geen vergunning aanwezig is&comma; dan kan de verhuurder een forse bestuurlijke boete opgelegd worden. Deze boete kan oplopen tot EUR 20.000. U zult begrijpen dat deze situatie voor verhuurder volkomen onacceptabel is.,
    },
  ]
---
