---
__id: 1
icon: reparatieverzoek.png
title: Reparatieverzoek
email: gj.somer@cornerstonevastgoed.nl
text_1: Het verzoek voor een reparatie of het melden van een storing kan via Ziezodan. Ziezodan is een platvorm die huurder en onderhoudspartijen rechtstreeks samenbrengt, waardoor de behandeling en afhandeling geoptimaliseerd is.
image: 'reparatieverzoek.jpg'
text_2: <div class="bold">Hoe werkt Ziezodan?</div><br/>Scan de aanwezige QR-code of stuur via Whatsapp het woord ‘storing’ naar 088-4000400 en volg de opvolgende instructies. Bij een spoedmelding kan je direct contact opnemen met de klantenservice van Ziezodan middels 088-4000400, welke 24/7 bereikbaar zijn. Via www.ziezodan.nl/huurders (koppeling) is een informatievideo beschikbaar waarin de werkwijze wordt uitgelegd.
text_3: <div class="bold">Belangrijk:</div><br/>Heeft u een afwijkend huisnummer, gedeelde deurbel of iets afwijkends wat de servicemonteur moet weten om u goed te kunnen bereiken, zet deze informatie dan bij uw melding in Ziezodan bij het kopje opmerkingen. Zo weet u zeker dat de servicemonteur bij de juiste deur aan de juiste bel drukt en voorkomen wij de “niet thuissituaties”. Heeft u meerdere reparatieverzoeken tegelijk? Doe elk reparatieverzoek in een eigen aparte melding en voeg hier duidelijke foto’s aan toe. Er kan namelijk maar één servicepartij per melding worden aangestuurd, zo weet u zeker dat de juiste servicepartij voor de juiste onderhoudsmelding aan uw deur komt.
text_4:
---
