---
__id: 7
icon: huurdersportaal.png
title: Onderhoud
text_1: Het is belangrijk dat je met een goed en veilig gevoel van je gehuurde woning kan genieten. Hierbij hoort ook onderhoud, welke de woning in goede conditie houdt en overlast (in de toekomst) voorkomt.
text_2: In de <a class="bold" target="_blank" href="/documents/cornerstone_huurwijzer.pdf">huurwijzer</a> is terug te lezen wie er verantwoordelijk is voor onderhoud en reparaties aan je woning. Er zijn werkzaamheden die we van jou als huurder verwachten, zoals het schoonhouden van de woning, voorkomen van verstoppingen en schade. Andere zaken zullen via ons verzorgd worden (denk aan groot onderhoud). Naast de verantwoording is er ook een <a target="_blank" class="bold" href="/documents/cornerstone_huurwijzer.pdf#page=21">wettelijke verdeling</a>.
---
