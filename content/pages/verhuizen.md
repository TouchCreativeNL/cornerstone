---
__id: 3
icon: verhuizen.png
title: Verhuizen
text_1: Bij verhuisplannen komt een hoop kijken, waaronder het beëindigen van de huurovereenkomst.
text_2: Graag ontvangen wij de opzegging via de <a class="bold" target="_blank" href="https://login.cornerstonevastgoed.nl/">huurdersportaal</a>, met inachtneming van een opzeggingstermijn van ten minste 1 volle kalendermaand. Bijvoorbeeld als je je huurovereenkomst per 30 juni wilt beëindigen, dan de moet de huuropzegging uiterlijk 31 mei door ons zijn ontvangen.
text_4: Na het beëindigen van de huurovereenkomst komen er doorgaans veel werkzaamheden op je pad, waaronder het opleveren van de woning. Hiervoor dienen twee afspraken te worden gemaakt, te weten de 1) voor- en 2) eindinspectie. Tijdens de voorinspectie komt een medewerker van Cornerstone langs om gezamenlijk de woning te controleren aan de hand van het opgestelde rapport bij aanvang van de huurovereenkomst. Eventuele opmerkingen, gebreken en herstelverzoeken worden besproken en genoteerd in een rapport, zodat alle partijen weten welke werkzaamheden er dienen te worden uitgevoerd voor de daadwerkelijke oplevering van de woning.
text_5: Tijdens de tweede afspraak inspecteert een medewerker van Cornerstone samen met jou de woning en bekijkt of de eerder overeengekomen werkzaamheden zijn uitgevoerd. Is de gewenste oplevering niet het geval dan zal Cornerstone de werkzaamheden alsnog laten uitvoeren waarvan de kosten in mindering worden gebracht op de aanwezige waarborgsom.
text_6: Bekijk ook eens de handige <a class="bold" target="_blank" href="/documents/cornerstone_verhuizen.pdf#page=6">tips</a> voorafgaand aan de verhuizing en uiteraard zijn wij bereikbaar voor vragen op <a class="bold" href="tel:085-4884780">085-4884780</a> of via <a class="bold" href="mailto:info@cornerstonevastgoed.nl">info@cornerstonevastgoed.nl</a>.
---
