---
__id: 5
icon: documenten.png
title: Documenten
text_1: Onderstaande buttons geven rechtstreeks toegang tot de diverse documenten waar o.a. naar wordt verwezen op de website.
documents:
  [
    { name: 'Burenoverlast', document: 'cornerstone_burenoverlast.pdf' },
    { name: 'Verhuizen', document: 'cornerstone_verhuizen.pdf' },
    { name: 'Vocht Schimmel', document: 'cornerstone_vochtschimmel.pdf' },
    {
      name: 'Reparaties herstellingen',
      document: 'cornerstone_reparatiesherstellingen.pdf',
    },
    { name: 'Schoonmaken', document: 'cornerstone_schoonmaken.pdf' },
    { name: 'Schilderwerk', document: 'cornerstone_schilderwerk.pdf' },
    {
      name: 'Machtiging automatische incasso formulier',
      document: 'machtiging_automatische_incasso_sepa_cornerstone_vastgoed.pdf',
    },
    {
      name: 'Algemene bepalingen woonruimte 2003 - 2017',
      document: 'algemene_bepalingen_woonruimte_2003-2017.zip',
    },
  ]
---
