import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _38288370 = () => interopDefault(import('../pages/algemene-voorwaarden/index.vue' /* webpackChunkName: "pages/algemene-voorwaarden/index" */))
const _f6dd5022 = () => interopDefault(import('../pages/contact/index.vue' /* webpackChunkName: "pages/contact/index" */))
const _9733582a = () => interopDefault(import('../pages/documenten/index.vue' /* webpackChunkName: "pages/documenten/index" */))
const _e5530536 = () => interopDefault(import('../pages/faq/index.vue' /* webpackChunkName: "pages/faq/index" */))
const _238fc1ce = () => interopDefault(import('../pages/huurdersportaal/index.vue' /* webpackChunkName: "pages/huurdersportaal/index" */))
const _895a8112 = () => interopDefault(import('../pages/onderhoud/index.vue' /* webpackChunkName: "pages/onderhoud/index" */))
const _42f8eb19 = () => interopDefault(import('../pages/privacy-verklaring/index.vue' /* webpackChunkName: "pages/privacy-verklaring/index" */))
const _175a92a8 = () => interopDefault(import('../pages/reparatieverzoek/index.vue' /* webpackChunkName: "pages/reparatieverzoek/index" */))
const _04c02bd9 = () => interopDefault(import('../pages/verhuizen/index.vue' /* webpackChunkName: "pages/verhuizen/index" */))
const _11af0b1e = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/algemene-voorwaarden",
    component: _38288370,
    name: "algemene-voorwaarden"
  }, {
    path: "/contact",
    component: _f6dd5022,
    name: "contact"
  }, {
    path: "/documenten",
    component: _9733582a,
    name: "documenten"
  }, {
    path: "/faq",
    component: _e5530536,
    name: "faq"
  }, {
    path: "/huurdersportaal",
    component: _238fc1ce,
    name: "huurdersportaal"
  }, {
    path: "/onderhoud",
    component: _895a8112,
    name: "onderhoud"
  }, {
    path: "/privacy-verklaring",
    component: _42f8eb19,
    name: "privacy-verklaring"
  }, {
    path: "/reparatieverzoek",
    component: _175a92a8,
    name: "reparatieverzoek"
  }, {
    path: "/verhuizen",
    component: _04c02bd9,
    name: "verhuizen"
  }, {
    path: "/",
    component: _11af0b1e,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
