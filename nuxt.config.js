export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',
  server: {
    port: 3333,
  },
  // Global page headers (https://go.nuxtjs.dev/config-head)
  router: {
    prefetchLinks: false,
    base: '/',
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition
      }

      return { x: 0, y: 0 }
    },
  },
  head: {
    htmlAttrs: {
      lang: 'nl',
    },
    title: 'Cornerstone Vastgoed',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0, viewport-fit=cover',
      },
      {
        hid: 'description',
        name: 'description',
        content:
          'Bij Cornerstone Vastgoed staat de huurder centraal richting een optimale klantbeleving voor alle partijen',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        src: '/scripts/script.js',
        body: true,
        defer: true,
      },
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['@/assets/styles/main.scss', '@/assets/fonts/avenir/avenir.scss'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    [
      'nuxt-compress',
      {
        gzip: {
          cache: true,
        },
        brotli: {
          threshold: 102400,
        },
      },
    ],
    ['@nuxtjs/html-minifier', { log: 'once', logHtml: true }],
    ['@nuxtjs/component-cache', { maxAge: 1000 * 60 * 60 }],
  ],

  // Content module configuration (https://go.nuxtjs.dev/content-config)
  content: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    analyze: false,
    /*
     ** You can extend webpack config here
     */
   
  },
}
